from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def bob11():
    return "Hello BOB from user090"

@app.route("/add")
def add():
    a = int(request.args.get('a', 0))
    b = int(request.args.get('b', 0))
    return str(a + b)

@app.route("/sub")
def sub():
    a = int(request.args.get('a', 0))
    b = int(request.args.get('b', 0))
    return str(a - b)

if __name__ == "__main__":
    app.run('0.0.0.0', port=8090)