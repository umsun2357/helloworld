import unittest
import requests

URL = "http://13.209.15.210:8090"
URL_ADD = URL + '/add'
URL_SUB = URL + '/sub'

class TestApp(unittest.TestCase):
    def test_add(self):
        params = {"a": "2", "b": "3"}
        res = requests.get(URL_ADD, params=params)
        out = int(res.text)
        self.assertEqual(out, 5)
    
    def test_sub(self):
        params = {"a": "2", "b": "3"}
        res = requests.get(URL_SUB, params=params)
        out = int(res.text)
        self.assertEqual(out, -1)

if __name__ == "__main__":
    unittest.main()